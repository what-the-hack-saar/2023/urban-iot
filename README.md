![alt text](docs/logo.jpg "UrbanIoT")
# UrbanIoT
Stationäre Sensoren messen nur die Umweltdaten an einem bestimmten Ort. Will man also die Umweltdaten einer ganzen Stadt erfassen, braucht man viele Sensoren, was teuer, aufwendig in der Wartung ist und, wie oben erwähnt, nur die Umweltdaten eines Ortes in der Stadt erfasst.

Das Ziel des Projekts ist es ein kostengünstiges mobiles Umweltüberwachungssystem zu schaffen, das auf einem Rucksack, E-Scooter, Fahrrad oder sogar einem Elektrofahrzeug montiert werden kann, sodass mit nur wenigen Sensoren die Umweltdaten einer ganzen Stadt erfasst werden können. Das System kann die Umgebungsdaten wie Luftfeuchtigkeit, aber auch den Verkehr, die LoRaWAN-Abdeckung und mehr einfach erfassen. Das Grove-Sensor-System ermöglicht es den Benutzer*innen, die Sensoren an die eignen Bedürfnisse anzupassen.

# SAAR HACKATHON #03 - Machen statt meckern!
Die Abschlusspräsentation gibt es hier: [Presentation_UrbanIoT.pdf](./Presentation_UrbanIoT.pdf)
## Das wollten wir machen • Firmware reparieren/umschreiben
- Umziehen auf ein Zepyhr RTOS von Arduino PlatfromIO ✅
    - Mehr Modularität
    - Umziehen auf neue Hardware
    - Die alte Hardware passte nicht für das Vorhaben
- Backend von Azure auf Open Source ✅
    - TTN
    - InfluxDB 
    - Grafana

## Design
![alt text](docs/wesbite_layout.jpg "Website layout")

## Architektur
![alt text](docs/architecture.png "Architektur der Anwendung")

## Team
Isabella, Monika, Lukas, Pascal, Robin, Rafael, Sebastian, Tobias, Philipp

## Danke!
Vielen Dank an die alle Organisierenden, Helfenden, Sponsoren\*innen und alle anderen 🙏